import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GmapNearbyComponent } from './gmap-nearby/gmap-nearby.component';

const routes: Routes = [
  {
    path: '',
    component: GmapNearbyComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
