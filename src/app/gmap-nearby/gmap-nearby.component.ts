import { Component, OnInit } from '@angular/core';

declare var google;

@Component({
  selector: 'app-gmap-nearby',
  templateUrl: './gmap-nearby.component.html',
  styleUrls: ['./gmap-nearby.component.css']
})
export class GmapNearbyComponent implements OnInit {

  mapkey = 'AIzaSyDVU0b1M6aNsetB-1cOBtMdv-9I5-1IN7I';
  url = 'https://maps.googleapis.com/maps/api/js?key=' + this.mapkey + '&callback=initMap&libraries=places';
  constructor() { }

  ngOnInit(): void {
    this.loadScript();
  }

  // loading the script on component level
  loadScript() {
    window['initMap'] = () => {
      this.initMap();
    };
    if (!window.document.getElementById('script')) {
      let node = document.createElement('script');
      node.src = this.url;
      node.type = 'text/javascript';
      window.document.body.appendChild(node);
    } else {
      this.initMap();
    }
  }

  initMap = () => {
    let map: any;
    let service: any;

    var pyrmont = new google.maps.LatLng(-33.8665433, 151.1956316);

    map = new google.maps.Map(document.getElementById("map"), {
      center: pyrmont,
      zoom: 16,
    });

    const cityCircle = new google.maps.Circle({
      strokeColor: "#FF0000",
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: "#FF0000",
      fillOpacity: 0.35,
      map,
      center: pyrmont,
      radius: 500,
    });

    cityCircle.setMap(map)

    // nearby search request 
    var request = {
      location: pyrmont,
      radius: '500',
      type: ['restaurant']
    };

    service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, (results, status) => {
      if (status === google.maps.places.PlacesServiceStatus.OK) {
        for (let i = 0; i < results.length; i++) {
          this.createMarker(results[i], map);
        }
      }
    });
  }

  // create markers on map based on positions
  createMarker(place, map) {
    const marker = new google.maps.Marker({
      map,
      position: place.geometry.location,
    });
  }

}
