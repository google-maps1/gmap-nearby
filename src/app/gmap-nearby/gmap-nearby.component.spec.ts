import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GmapNearbyComponent } from './gmap-nearby.component';

describe('GmapNearbyComponent', () => {
  let component: GmapNearbyComponent;
  let fixture: ComponentFixture<GmapNearbyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GmapNearbyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GmapNearbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
